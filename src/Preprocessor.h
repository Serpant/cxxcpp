#ifndef CXXPP_PREPROCESSOR_H
#define CXXPP_PREPROCESSOR_H

#include <fstream>
#include <map>
#include <optional>
#include <stack>
#include <string>

#include "IncludedFile.h"

class Preprocessor {
public:
    explicit Preprocessor(const std::string& filename);

    void parseMacro(const std::string& line);
    std::string parseLine(std::string&& line);
    bool add_variable(const std::string& new_name, const std::string& new_value);
    void includeFile(const std::string& filename);

    static constexpr auto extension = ".pre";
    
    std::string createErrorMsg(const std::string& msg);
    void warning_msg(const std::string& msg);

    IncludedFile& currentFile() { return includedFiles_.top(); }
    std::string getVariable(const std::string& name);
private:
    void handleMacroDefinition(std::istringstream& input_line);
    void parseFile();

    std::map<std::string, std::string> variables_;
    std::ofstream resultFile_;

    std::stack<IncludedFile> includedFiles_;
};

class SyntaxError {
public:
    explicit SyntaxError(const std::string& errorMsg): errorMsg_{errorMsg} { }
    std::string what() const { return errorMsg_; }
private:
    std::string errorMsg_;
};

constexpr auto getPreprocessorDirectives();

std::string getIncludedFileName(const std::string& line);

bool isValidDirective(const std::string& directive);

#endif  // CXXPP_PREPROCESSOR_H
