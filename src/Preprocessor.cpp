#include "Preprocessor.h"

#include <algorithm>
#include <array>
#include <iostream>
#include <optional>
#include <regex>
#include <sstream>
#include <string_view>

Preprocessor::Preprocessor(const std::string& filename)
    : resultFile_{filename + extension}
{
    if (!resultFile_) {
        throw std::runtime_error{"cannot open file '" + filename + "' for output"};
    }
}

std::string Preprocessor::getVariable(const std::string& name)
{
    auto var = variables_.find(name);
    if (var != variables_.end())
        return var->second;
    else
        throw std::runtime_error{"Variable with name " + name + " is not defined"};
}

constexpr auto getPreprocessorDirectives() 
{
    constexpr std::array<std::string_view, 2> directives = {
        "#include",
        "#define"
        //"#ifdef",
        //"#ifndef"
        //"#endif"
    };

    return directives;
}

// returns true if variable was added
// false if not
bool Preprocessor::add_variable(const std::string& new_name,
                                const std::string& new_value) 
{
    if (new_name.empty())
        throw SyntaxError{ createErrorMsg("Syntax Error: no variable name in #define") };

    const auto[pos, is_inserted] = variables_.insert(make_pair(new_name, new_value));
    const auto&[old_name, old_value] = *pos;

    // accept multiple definition with the same value assigned
    if (!is_inserted && (old_value != new_value))
        return false;

    return true;
}

void Preprocessor::handleMacroDefinition(std::istringstream& input_line)
{
    std::string new_name, new_value;
    input_line >> new_name >> new_value;

    bool is_added = add_variable(new_name, new_value);
    if (!is_added) {
        const auto&[name, value] = *(variables_.find(new_value));
        const std::string warning =
            "redefiniton attempt of variable " + name + "\n\tvalue '" + value +
            "', new proposed value '" + new_value + "'\n";
        warning_msg(warning);
    }
}

std::string getIncludedFileName(const std::string& line)
{
    constexpr int filename_index = 1;
    std::smatch filename_match;
    std::regex_match(line, filename_match, 
                    std::regex{R"(^#include +[<"](\w+(\.\w+)?)[">] *$)"});

    std::string filename = filename_match[filename_index];
    return filename;
}

// warning print to user when parsing file
void Preprocessor::warning_msg(const std::string& msg)
{
    std::ostringstream error_message;
    error_message << "WARNING: "<< includedFiles_.top().getFileName() << ':'
                  << includedFiles_.top().getCurrLine() << ' ' << msg;

    std::cerr << error_message.str() << '\n';
}

// error print to user and to file to prevent usage of output file
std::string Preprocessor::createErrorMsg(const std::string& msg)
{
    std::ostringstream error_message;
    error_message << includedFiles_.top().getFileName() << ':'
                  << includedFiles_.top().getCurrLine() << ' ' << msg;
    return error_message.str();
}

void Preprocessor::parseMacro(const std::string& line)
{
    std::istringstream input_line{line};

    std::string macro;
    input_line >> macro;
    if (macro == "#define")
        handleMacroDefinition(input_line);
    else if (macro == "#include") {
        auto filename = getIncludedFileName(line);
        includeFile(filename);
    } else {
        throw SyntaxError{ createErrorMsg("Unknown directive: " + line) };
    }
}

// returns parsed line
std::string Preprocessor::parseLine(std::string&& line)
{
    for (const auto&[var_name, var_value] : variables_) {
        auto pos = line.find(var_name);
        while (pos != std::string::npos) {
            line.replace(pos, var_name.size(), var_value);
            pos = line.find(var_name);
        }
    }
    return line;
}

bool isMacro(const std::string& line) 
{
    std::regex macro_pattern{R"(^#.+)"};

    if (std::regex_match(line, macro_pattern))
        return true;

    return false;
}

void Preprocessor::parseFile()
{
    for (std::string line; includedFiles_.top().getline(line); ) {
        if (isMacro(line)) {
            parseMacro(line);
        } else {
            resultFile_ << parseLine( std::move(line) ) << '\n';
        }
    }
}

void Preprocessor::includeFile(const std::string& filename)
{
    if (filename.empty())
        throw SyntaxError{"no file name in #include"};

    IncludedFile file{filename};
    if (!file.isOpen())
        throw SyntaxError{
            createErrorMsg("Cannot open file named '" + filename + "'")
        };

    includedFiles_.push(std::move(file));
    parseFile();
    includedFiles_.pop();
}
