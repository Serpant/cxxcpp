#include "IncludedFile.h"

IncludedFile::IncludedFile(const std::string& filename)
:
    filename_{filename},
    file_{filename}
{
}

const std::ifstream& IncludedFile::getline(std::string& line)
{
    std::getline(file_, line);
    ++line_number_;

    return file_;
}
