#include "read_arguments.h"

#include <string>
#include <vector>

auto read_arguments(int argc, const char* argv[]) -> std::vector<std::string>
// skips first position because it is path to the current program
// returns empty vector if no arguments given
{
    using std::string;
    using std::vector;
    std::vector<std::string> arguments;

    constexpr int first_argument = 1;
    for (int i = first_argument; i < argc; ++i) 
        arguments.push_back(argv[i]);

    return arguments;
}
