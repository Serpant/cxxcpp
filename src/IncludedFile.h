#ifndef CXXPP_INCLUDED_FILE_H
#define CXXPP_INCLUDED_FILE_H

#include <fstream>
#include <string>

class IncludedFile {
public:
    IncludedFile(const std::string& filename);

    const std::ifstream& getline(std::string& line);
    auto getFileName() const { return filename_; }
    auto getCurrLine() const { return line_number_; }

    bool isOpen() { 
        return file_ ? true : false;
    }
private:
    std::string filename_;
    std::ifstream file_;
    int line_number_ = 0;
};

#endif // CXXPP_INCLUDED_FILE_H
