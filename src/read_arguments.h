#ifndef CXXCPP_READ_ARGUMENTS
#define CXXCPP_READ_ARGUMENTS

#include <string>
#include <vector>

auto read_arguments(int argc, const char* argv[]) -> std::vector<std::string>;

#endif  // CXXCPP_READ_ARGUMENTS
