/*
 * project name: cxxpp -> c++ preprocessor
 */
#include <assert.h>

#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <string_view>
#include <type_traits>
#include <vector>

#include "Preprocessor.h"
#include "read_arguments.h"

int main(int argc, const char* argv[])
try {
    auto arguments = read_arguments(argc, argv);
    if (arguments.empty())
        throw std::runtime_error{"no filename where given"};
    else if (arguments.size() > 1)
        throw std::runtime_error{"too much file names given"};

    Preprocessor cxxpp{arguments.front()};
    cxxpp.includeFile(arguments.front());
}
catch (const SyntaxError& e) {
    std::cerr << e.what() << '\n';
    return 126;
}
catch (std::exception& e) {
    std::cerr << e.what() << '\n';
    return -1;
}
catch (...) {
    std::cerr << "Unknown error\n";
    return -2;
}
